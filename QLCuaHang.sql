﻿use master
go 
declare @Sql varchar(1000), @databasename varchar(100) 
set @databasename = 'QLCuaHang1' 
set @Sql = ''  
select  @Sql = @Sql + 'kill ' + convert(char(10), spid) + ' ' 
from    master.dbo.sysprocesses 
where   db_name(dbid) = @databasename
     and 
     dbid <> 0 
     and 
     spid <> @@spid 
exec(@Sql)
GO
drop database QLCuaHang1
go
CREATE DATABASE QLCuaHang1
go
USE QLCuaHang1


/*________========TẠO BẢNG========________*/
go
create table tbl_user(
	id int primary key identity(1,1),
	username varchar(10),
	password varchar(100),
	lanDangNhap int default 0
)
go
insert into tbl_user (username,password, lanDangNhap) values ('admin', 'admin' , 0),('admin2', 'admin2' , 0)
go
CREATE TABLE tbl_KhachHang(
	MaKH NVARCHAR(50) NOT NULL PRIMARY KEY,
    TenKH NVARCHAR(50) NULL,
	GioiTinh NVARCHAR(50) NULL,
	NgaySinh datetime NULL,
    DienThoai NVARCHAR(50) NULL,
    DiaChi NVARCHAR(50) NULL

);

CREATE TABLE tbl_NhanVien(
	MaNV int NOT NULL PRIMARY KEY identity(1,1),
	TenNV NVARCHAR(50) NULL,
	GioiTinh NVARCHAR(50) NULL,
	NgaySinh datetime NULL,
	Sdt NVARCHAR(50) NULL,
	DiaChi NVARCHAR(50) NULL
);

CREATE TABLE tbl_HangHoa (
	Mahang NVARCHAR (50) NOT NULL PRIMARY KEY,
    TenHang NVARCHAR(50) NOT NULL,
	SoLuongHang INT NULL,
    Gia FLOAT NULL,
	DonViTinh NVARCHAR(50) NULL,
);

CREATE TABLE tbl_HoaDon(
	MaHD NVARCHAR (50) NOT NULL PRIMARY KEY,
	ThoiGianLap datetime,
	ThoiGianThanhToan datetime,
	MaNV int,
	MaKH NVARCHAR(50),
	TongTien float,
	FOREIGN KEY (MaNV) REFERENCES tbl_NhanVien(MaNV),
	FOREIGN KEY (MaKH) REFERENCES tbl_KhachHang(MaKH)
);

CREATE TABLE tbl_CTHD(
	MaHD NVARCHAR (50) NOT NULL,
	Mahang NVARCHAR (50) NOT NULL,
	SoLuong int,
);

ALTER TABLE tbl_CTHD ADD CONSTRAINT PK_CTHD PRIMARY KEY (MaHD,Mahang);

ALTER TABLE tbl_CTHD
ADD CONSTRAINT FK_cthd
FOREIGN KEY (MaHD) REFERENCES tbl_HoaDon(MaHD);

ALTER TABLE tbl_CTHD
ADD CONSTRAINT FK_cthd_hanghoa
FOREIGN KEY (Mahang) REFERENCES tbl_HangHoa(Mahang);

insert tbl_NhanVien  ( TenNV,  GioiTinh,NgaySinh,  Sdt,DiaChi) values ('Cao Duc Duong','Nam','8/23/1998','0969304401','Hai Duong');
insert tbl_NhanVien   ( TenNV,  GioiTinh,NgaySinh,  Sdt,DiaChi) values ('Nguyen Lan Huong','Nu','6/9/1998','0963882551','Quang Ninh');

insert tbl_KhachHang values('KH01','Vu Viet Hoang','Nam','2/3/1998','0946682271','Hai Duong');
insert tbl_KhachHang values('KH02','Hoang Kim Lien','Nu','1/13/1999','0963876441','Ha Noi');

insert tbl_HangHoa values ('MH1','Redmi 5Plus','5','4000000','VND');
insert tbl_HangHoa values ('MH2','IPhone X','7','21000000','VND');

insert tbl_HoaDon values ('1','2/1/2019','4/10/2019','1','KH01','8000000');
insert tbl_HoaDon values ('2','2/1/2019','4/1/2019','1','KH02','21000000');

--insert tbl_CTHD values ('1','MH1','2');
--insert tbl_CTHD values ('2','MH2','1');

--delete tbl_NhanVien
--delete tbl_KhachHang
--delete tbl_HangHoa
--delete tbl_HoaDon
--delete tbl_CTHD

select* from tbl_Nhanvien
select * from tbl_HangHoa
select * from tbl_KhachHang
select * from tbl_HoaDon
select * from tbl_CTHD

--proc thực hiện các thao tác với bảng tbl_NhanVien
--proc select_NhanVien
go
create proc select_NhanVien
as
select * from tbl_NhanVien
go
--proc add_NhanVien
create proc add_NhanVien
@manv int = null,
@tennv nvarchar(50) = null,
@ngaysinh datetime = null,
@gioitinh nvarchar(50) = null,
@diachi nvarchar(50) = null,
@sdt nvarchar(50) = null
as
insert into tbl_NhanVien( TenNV, NgaySinh, GioiTinh, DiaChi, Sdt) values ( @tennv, @ngaysinh, @gioitinh,  @diachi, @sdt)
go
--proc update_NhanVien
create proc update_NhanVien
@manv int = null,
@tennv nvarchar(50) = null,
@ngaysinh datetime = null,
@gioitinh nvarchar(50) = null,
@diachi nvarchar(50) = null,
@sdt nvarchar(50) = null
as
update tbl_NhanVien set TenNV = @tennv, NgaySinh = @ngaysinh, GioiTinh = @gioitinh, DiaChi = @diachi, Sdt = @sdt where MaNV = @manv
go
--proc delete_NhanVien
create proc delete_NhanVien
@manv int = null
as
delete from tbl_NhanVien where MaNV = @manv
go
--proc delete_HoaDon
create proc delete_HoaDon_MaNv
@manv int = null
as
delete from tbl_HoaDon where MaNV = @manv

go

--tìm kiếm NhanVien
create PROC timkiemNV
@noidung nvarchar(50) = null
AS
BEGIN 
	select * from tbl_NhanVien
			where TenNV like '%' + @noidung + '%' 
			or GioiTinh like '%' + @noidung + '%' 
			or DiaChi like '%' + @noidung + '%'
			or Sdt like '%' + @noidung + '%'
END
go
--proc thực hiện các thao tác với bảng tbl_KhachHang
--proc select_KhachHang
create proc select_KhachHang
as
select * from tbl_KhachHang
go
--proc add_KhachHang
create proc add_KhachHang
@makh nvarchar(50) = null,
@tenkh nvarchar(50) = null,
@ngaysinh datetime = null,
@gioitinh nvarchar(50) = null,
@diachi nvarchar(50) = null,
@sdt nvarchar(50) = null
as
insert into tbl_KhachHang(MaKH, TenKH, NgaySinh, GioiTinh, DiaChi, DienThoai) values (@makh, @tenkh, @ngaysinh, @gioitinh,  @diachi, @sdt)
go
--proc update_KhachHang
create proc update_KhachHang
@makh nvarchar(50) = null,
@tenkh nvarchar(50) = null,
@ngaysinh datetime = null,
@gioitinh nvarchar(50) = null,
@diachi nvarchar(50) = null,
@sdt nvarchar(50) = null
as
update tbl_KhachHang set TenKH = @tenkh, NgaySinh = @ngaysinh, GioiTinh = @gioitinh, DiaChi = @diachi, DienThoai = @sdt where MaKH = @makh
go
--proc delete_KhachHang
create proc delete_KhachHang
@makh nvarchar(50) = null
as
delete from tbl_KhachHang where MaKH = @makh
go
--tìm kiếm khach hang
create PROC timkiemKH
@noidung nvarchar(50) = null
AS
BEGIN 
	select * from tbl_KhachHang 
			where TenKH like '%' + @noidung + '%' 
			or MaKH like '%' + @noidung + '%' 
			or GioiTinh like '%' + @noidung + '%' 
			or DiaChi like '%' + @noidung + '%'
			or DienThoai like '%' + @noidung + '%'
END

go
---proc thực hiện các thao tác  với bảng Mathang
---proc select_HangHoa
create proc select_HangHoa
as
select * from tbl_HangHoa 
---proc select_HangHoa theo giá
go
create proc DSHHtheoGia
@gia float
as
select * from tbl_HangHoa where Gia <  @gia
---proc add_HangHoa
go
create proc add_HangHoa
@MaHang int = NULL,
@TenHang NVARCHAR(50) = NULL,
@SoLuongHang INT = NULL,
@DonViTinh NVARCHAR(50) = NULL,
@Gia float = NULL
as
	insert into tbl_HangHoa(Mahang,TenHang, SoLuongHang,DonViTinh,Gia) 
	values (@MaHang,@TenHang, @SoLuongHang, @DonViTinh, @Gia)
go
---proc update_HangHoa
create proc update_HangHoa
@MaHang int = NULL,
@TenHang NVARCHAR(50) = NULL,
@SoLuongHang INT = NULL,
@DonViTinh NVARCHAR(50) = NULL,
@Gia float = NULL
as
		update tbl_HangHoa 
		set TenHang =  @TenHang, SoLuongHang = @SoLuongHang, DonViTinh= @DonViTinh, Gia= @Gia
		where MaHang = @MaHang
go	
---proc delete_HangHoa
create proc delete_HangHoa
@MaHang int = NULL
as
	delete from tbl_HangHoa where MaHang = @MaHang
go
--tìm kiếm HangHoa
create PROC timkiemHH
@noidung nvarchar(50) = null
AS
BEGIN 
	select * from tbl_HangHoa 
			where Mahang like '%' + @noidung + '%' 
			or TenHang like '%' + @noidung + '%' 
			or Gia like '%' + @noidung + '%'
			or SoLuongHang like '%' + @noidung + '%'
END
go
------------------------------------------------------------------------
-- load DS Hóa đơn
create proc ds_hoadon
as
SELECT tbl_HoaDon.MaHD,TenKH,TenHang,SoLuong,Gia,TongTien FROM tbl_HoaDon
 inner join tbl_CTHD On tbl_HoaDon.MaHD = tbl_CTHD.MaHD
 inner join tbl_HangHoa on tbl_CTHD.Mahang = tbl_HangHoa.Mahang
 inner join tbl_KhachHang on tbl_HoaDon.MaKH = tbl_KhachHang.MaKH

 exec ds_hoadon

 Select * from tbl_HoaDon
 go
-- Thêm Hóa Đơn
create proc add_HoaDon
@mahd nvarchar(50) = NULL,
@manv int= NULL,
@makh nvarchar(50) = NULL,
@mahang nvarchar(50) = NULL,
@soluong int = NULL
as
begin
insert into tbl_HoaDon (MaHD,MaNV,MaKH) values (@mahd,@manv,@makh)
insert into tbl_CTHD(MaHD,Mahang,SoLuong) values (@mahd,@mahang,@soluong)
end

exec add_HoaDon '10','01','KH01','MH2',20
select * from tbl_HoaDon
go
alter trigger Tongtien
on tbl_CTHD
after insert, update
as 
begin
	declare @soluong int
	declare @mahd nvarchar(50)
	select @mahd=mahd from inserted
	select @soluong = soluong from inserted 
	declare @mahang nvarchar(50)
	select @mahang = mahang from inserted
	declare @gia float
	select @gia = gia from tbl_HangHoa where @mahang = Mahang
	update tbl_HoaDon set TongTien = @gia * @soluong where tbl_HoaDon.MaHD=@mahd
	
end

select * from tbl_CTHD

select * from tbl_HangHoa

-- load tên NV lập hoá đơn
go
create PROC sp_tennhanvien
@MaNV int = null
as
begin
	select TenNV from tbl_NhanVien where MaNV= @MaNV
end
go
-- load khách hàng mua
create PROC sp_kh
as
begin
	Select * from tbl_KhachHang
end

select * from tbl_CTHD;
go
--Lay ten hang hoa---
CREATE PROC sp_loadtenhang
AS
BEGIN
	SELECT * FROM tbl_HangHoa 
END
go
-- trigger tự động lấy ngày hiện tại khi lập hóa đơn
create TRIGGER Tg_getdate
ON tbl_HoaDon
FOR INSERT
AS
	BEGIN
		DECLARE @iSoHD int
		select @iSoHD = (select MaHD from inserted)
		UPDATE tbl_HoaDon 
		SET ThoiGianLap = GETDATE()
		where MaHD = @iSoHD
	END

 SELECT * FROM dbo.tbl_HoaDon
