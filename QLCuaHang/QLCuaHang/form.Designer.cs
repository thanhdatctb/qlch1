﻿namespace QLCuaHang
{
    partial class form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rpDSHH = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // rpDSHH
            // 
            this.rpDSHH.ActiveViewIndex = -1;
            this.rpDSHH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rpDSHH.Cursor = System.Windows.Forms.Cursors.Default;
            this.rpDSHH.Location = new System.Drawing.Point(1, 2);
            this.rpDSHH.Name = "rpDSHH";
            this.rpDSHH.Size = new System.Drawing.Size(884, 394);
            this.rpDSHH.TabIndex = 0;
            // 
            // InDSHH
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 461);
            this.Controls.Add(this.rpDSHH);
            this.Name = "InDSHH";
            this.Text = "InDSHH";
            this.Load += new System.EventHandler(this.InDSHH_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer rpDSHH;
    }
}