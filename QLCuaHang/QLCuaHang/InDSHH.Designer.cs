﻿namespace QLCuaHang
{
    partial class InDSHH
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rpindshh = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // rpindshh
            // 
            this.rpindshh.ActiveViewIndex = -1;
            this.rpindshh.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rpindshh.Cursor = System.Windows.Forms.Cursors.Default;
            this.rpindshh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rpindshh.Location = new System.Drawing.Point(0, 0);
            this.rpindshh.Name = "rpindshh";
            this.rpindshh.Size = new System.Drawing.Size(941, 453);
            this.rpindshh.TabIndex = 0;
            // 
            // InDSHH
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(941, 453);
            this.Controls.Add(this.rpindshh);
            this.Name = "InDSHH";
            this.Text = "InDSHH";
            this.Load += new System.EventHandler(this.InDSHH_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer rpindshh;
    }
}