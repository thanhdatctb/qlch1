﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLCuaHang
{
    public partial class NhanVien : Form
    {
        public NhanVien()
        {
            InitializeComponent();
        }
        KetnoiNhanvien kn = new KetnoiNhanvien();
        private void NhanVien_Load(object sender, EventArgs e)
        {
            dgvDansachNV.DataSource = kn.select_pro("select_NhanVien");
            dgvDansachNV.Columns[3].DefaultCellStyle.Format = "dd/MM/yyyy";
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            string ma = txtMaNV.Text;
            string ten = txtTenNV.Text;
            string sdt = txtSDT.Text;
            string diachi = txtDiachi.Text;
            string ngaysinh = dtNgaysinh.Value.ToString("dd/MM/yyyy");
            string gioitinh = rdNam.Checked ? "Nam" : "Nữ";
            if (txtMaNV.Text == "" || txtTenNV.Text == " ") 
            {
                MessageBox.Show("Chưa Nhập Mã Nhân Viên với Tên Nhân Viên");
                txtMaNV.Focus();
            }
            else
            {
                DataTable dt = new DataTable();
                dt = kn.Truyvan("select * from tbl_NhanVien where MaNV = '" + txtMaNV.Text + "'");
                if(dt.Rows.Count>0)
                {
                    MessageBox.Show("Đã tồn tại mã nhân viên");
                }
                else
                {
                    string gt = "Nam";
                    if(rdNam.Checked == true)
                    {
                        gt = "Nu";
                    }
                    kn.add_NhanVien(ma,ten, gioitinh, ngaysinh,sdt, diachi);
                    dgvDansachNV.DataSource = kn.select_pro("select_NhanVien");
                    xoachu();
                }
            }
        }

        private void dgvDansachNV_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int r = dgvDansachNV.CurrentCell.RowIndex;
            btnThem.Enabled = false;
            txtMaNV.Enabled = false;
            txtMaNV.Text = dgvDansachNV.Rows[r].Cells["MaNV"].Value.ToString();
            txtTenNV.Text = dgvDansachNV.Rows[r].Cells["TenNV"].Value.ToString();
            if(dgvDansachNV.Rows[r].Cells["GioiTinh"].Value.ToString()== "Nam")
            {
                rdNam.Checked = true;
            }
            else
            {
                rdNu.Checked = true;
            }
            dtNgaysinh.Text = dgvDansachNV.Rows[r].Cells["NgaySinh"].Value.ToString();
           
            txtSDT.Text = dgvDansachNV.Rows[r].Cells["Sdt"].Value.ToString();
            txtDiachi.Text = dgvDansachNV.Rows[r].Cells["DiaChi"].Value.ToString();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            string ma = txtMaNV.Text;
            string ten = txtTenNV.Text;
            string sdt = txtSDT.Text;
            string diachi = txtDiachi.Text;
            string ngaysinh = dtNgaysinh.Value.ToString("dd/MM/yyyy");
            string gioitinh = rdNam.Checked ? "Nam" : "Nữ";
            if (txtMaNV.Text == "" || txtTenNV.Text == " ")
            {
                MessageBox.Show("Chưa Nhập Mã Nhân Viên với Tên Nhân Viên");
                txtMaNV.Focus();
            }
            else
            {
                DataTable dt = new DataTable();
                {
                    string gt = "Nam";
                    if (rdNam.Checked == true)
                    {
                        gt = "Nu";
                    }
                    kn.update_NhanVien(ma, ten, gioitinh, ngaysinh, sdt, diachi);
                    dgvDansachNV.DataSource = kn.select_pro("select_NhanVien");
                    xoachu();
                }
            }
        }
        public void xoachu()
        {
            btnThem.Enabled = true;
            txtMaNV.Enabled = true;
            txtTenNV.ResetText();
            txtSDT.ResetText();
            txtDiachi.ResetText();
            rdNam.Checked = true;
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (txtMaNV.Text == "")
            {
                MessageBox.Show("Mời Bạn Nhập Mã Cần Xóa ");
                return;
            }
            DialogResult = MessageBox.Show("Xóa nhân viên sẽ xóa toàn bộ hóa đơn của nhân viên này","Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (DialogResult == DialogResult.Yes)
            {
                new KetnoiHoaDon().delete_HoaDon(txtMaNV.Text);
                kn.delete_NhanVien(txtMaNV.Text);
                dgvDansachNV.DataSource = kn.select_pro("select_NhanVien");
                xoachu();
                MessageBox.Show("Xóa thành công");
                // any function

            }
            else
            {
                // any function
                return;
            }
           
                
            
        }

        private void btnlammoi_Click(object sender, EventArgs e)
        {
            dgvDansachNV.DataSource = kn.select_pro("select_NhanVien");
            xoachu();
        }

        private void btntimkiem_Click(object sender, EventArgs e)
        {
            dgvDansachNV.DataSource = kn.timkiem_NhanVien(txttimkiem.Text);
        }

        private void txttimkiem_KeyUp(object sender, KeyEventArgs e)
        {
            btntimkiem_Click(sender, e);
        }
    }
}
