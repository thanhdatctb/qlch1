﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace QLCuaHang
{
    class KetnoiAdmin
    {
        //public static string Ketnoi = @"Data Source=DESKTOP-171AVQP\SQLEXPRESS;Initial Catalog=QLCuaHang1;Integrated Security=True";
        public static string Ketnoi = @"Data Source=DESKTOP-FSTUGSU\SQLEXPRESS;Initial Catalog=QLCuaHang1;Integrated Security=True";

        SqlConnection connection;
        DataTable dt;
        SqlDataAdapter da;
        SqlCommand command;
        public SqlConnection open()
        {
            connection = new SqlConnection(Ketnoi);
            if (connection.State == ConnectionState.Closed)
                connection.Open();
            return connection;
        }
        public SqlConnection closed()
        {
            connection = new SqlConnection(Ketnoi);
            if (connection.State == ConnectionState.Open)
                connection.Close();
            return connection;
        }
        public DataTable Truyvan(string sql)
        {
            dt = new DataTable();
            command = new SqlCommand(sql, connection);
            da = new SqlDataAdapter(command);
            da.Fill(dt);
            return dt;
        }
        public DataTable Laybang(string sql)
        {
            dt = new DataTable();
            open();
            da = new SqlDataAdapter(sql, Ketnoi);
            DataSet ds = new DataSet();
            da.Fill(dt);
            closed();
            return dt;
        }

        public DataTable select_pro(string tenproc)
        {
            dt = new DataTable();
            open();
            command = new SqlCommand(tenproc, connection);
            command.CommandType = CommandType.StoredProcedure;
            da = new SqlDataAdapter(command);
            da.Fill(dt);
            closed();
            return dt;
        }
        public SqlCommand add_HangHoa(string ma, string ten, string soluonghang, string donvitinh, string gia)
        {
            open();
            command = new SqlCommand("add_HangHoa", connection);
            command.Parameters.Add(new SqlParameter("@MaHang", ma));
            command.Parameters.Add(new SqlParameter("@TenHang", ten));
            command.Parameters.Add(new SqlParameter("@SoLuongHang", soluonghang));
            command.Parameters.Add(new SqlParameter("@DonViTinh", donvitinh));
            command.Parameters.Add(new SqlParameter("@Gia", gia));
            command.CommandType = CommandType.StoredProcedure;
            command.ExecuteNonQuery();
            closed();
            return command;
        }
        public SqlCommand update_HangHoa(string ma, string ten, string soluonghang, string donvitinh, string gia)
        {
            open();
            command = new SqlCommand("update_HangHoa", connection);
            command.Parameters.Add(new SqlParameter("@MaHang", ma));
            command.Parameters.Add(new SqlParameter("@TenHang", ten));
            command.Parameters.Add(new SqlParameter("@SoLuongHang", soluonghang));
            command.Parameters.Add(new SqlParameter("@DonViTinh", donvitinh));
            command.Parameters.Add(new SqlParameter("@Gia", gia));
            command.CommandType = CommandType.StoredProcedure;
            command.ExecuteNonQuery();
            closed();
            return command;
        }
        public void Loadcombobox ( ComboBox ten , string nametable, string conten, string cotma)
        {
            dt = new DataTable();
            dt = Truyvan("select * from " + nametable);
            ten.DataSource = dt;
            ten.ValueMember = cotma;
            ten.DisplayMember = conten;
        }
        public SqlCommand add_HoaDon(string mahd, string manv, string makh, string mahang, string soluong)
        {
            open();
            command = new SqlCommand("add_HoaDon", connection);
            command.Parameters.Add(new SqlParameter("@mahd", mahd));
            command.Parameters.Add(new SqlParameter("@manv", manv));
            command.Parameters.Add(new SqlParameter("@makh", makh));
            command.Parameters.Add(new SqlParameter("@mahang", mahang));
            command.Parameters.Add(new SqlParameter("@soluong", soluong));
            command.CommandType = CommandType.StoredProcedure;
            command.ExecuteNonQuery();
            closed();
            return command;
        }
        public SqlCommand DSHHtheoGia(float gia)
        {
            open();
            SqlCommand command = new SqlCommand("DSHHtheoGia", connection);
            command.Parameters.Add(new SqlParameter("@gia", gia));
            command.CommandType = CommandType.StoredProcedure;
            command.ExecuteNonQuery();
            closed();
            return command;
        }
        public bool LogIn(String username, String password)
        {
            try
            {
                String sql = "select * from tbl_user where username = @username and password = @password";
                open();
                SqlCommand command = new SqlCommand(sql, connection);
                command.Parameters.Add("username", username);
                command.Parameters.Add("password", password);
                var result = command.ExecuteReader();
                while (result.Read())
                {
                    int id = result.GetInt32(0);
                    MessageBox.Show(id.ToString());
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            

           
        }
    }
}
