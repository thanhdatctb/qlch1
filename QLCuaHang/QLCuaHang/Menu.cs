﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLCuaHang
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void mnuQuanLyNhanVien_Click(object sender, EventArgs e)
        {
            NhanVien f = new NhanVien();
            f.Show();
        }

        private void mnuQuanLyKhachHang_Click(object sender, EventArgs e)
        {
            KhachHang f = new KhachHang();
            f.Show();
        }

        private void mnuQuanLyHangHoa_Click(object sender, EventArgs e)
        {
            HangHoa f = new HangHoa();
            f.Show();
        }

        private void mnuThanhToan_Click(object sender, EventArgs e)
        {
            Thanh_Toán f = new Thanh_Toán();
            f.Show();
        }
    }
}
