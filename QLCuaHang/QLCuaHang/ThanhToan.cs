﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;

namespace QLCuaHang
{
    public partial class Thanh_Toán : Form
    {
        string maNV;
        int MaHD;
        public static string str = @"Data Source=DESKTOP-171AVQP\SQLEXPRESS;Initial Catalog=QLCuaHang1;Integrated Security=True";
        public Thanh_Toán()
        {
            InitializeComponent();
        }

        KetnoiHanghoa kn = new KetnoiHanghoa();
        private void Thanh_Toán_Load(object sender, EventArgs e)
        {
            dgvhoadon.DataSource = kn.select_pro("ds_hoadon");
            kn.Loadcombobox(cb_KhachHang, "tbl_KhachHang", "TenKH", "MaKH");
            kn.Loadcombobox(cb_NhanVien, "tbl_NhanVien", "TenNV", "MaNV");
            kn.Loadcombobox(cb_TenMatHang, "tbl_HangHoa", "TenHang", "Mahang");
        }

        private void btnInHD_Click(object sender, EventArgs e)
        {
            InDSHD f = new InDSHD();
            f.Show();
        }

        private void btn_luuthem_Click(object sender, EventArgs e)
        {
            string soluong = txtsoluong.Text;
            string mahd = txtmahoadon.Text;
            string manv = cb_NhanVien.SelectedValue.ToString();
            string makh = cb_KhachHang.SelectedValue.ToString();
            string mahang = cb_TenMatHang.SelectedValue.ToString();
            kn.add_HoaDon(mahd,manv,makh, mahang, soluong);
            dgvhoadon.DataSource = kn.select_pro("ds_hoadon");

        }

    }
}
