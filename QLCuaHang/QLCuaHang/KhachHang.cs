﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLCuaHang
{
    public partial class KhachHang : Form
    {
        public KhachHang()
        {
            InitializeComponent();
        }
        KetnoiKhachhang kn = new KetnoiKhachhang();
        private void KhachHang_Load(object sender, EventArgs e)
        {
            dgvDanhsachKH.DataSource = kn.select_pro("select_KhachHang");
            dgvDanhsachKH.Columns[3].DefaultCellStyle.Format = "dd/MM/yyyy";
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            string ma = txtMaKH.Text;
            string ten = txtTenKH.Text;
            string sdt = txtSDT.Text;
            string diachi = txtDiachi.Text;
            string ngaysinh = dtNgaysinh.Value.ToString("dd/MM/yyyy");
            string gioitinh = rdNam.Checked ? "Nam" : "Nữ";
            if (txtMaKH.Text == "" || txtTenKH.Text == " ")
            {
                MessageBox.Show("Chưa Nhập Mã Khách Hàng với Tên Khách Hàng");
                txtMaKH.Focus();
            }
            else
            {
                DataTable dt = new DataTable();
                dt = kn.Truyvan("select * from tbl_NhanVien where MaNV = '" + txtMaKH.Text + "'");
                if (dt.Rows.Count > 0)
                {
                    MessageBox.Show("Đã tồn tại mã khách hàng");
                }
                else
                {
                    string gt = "Nam";
                    if (rdNam.Checked == true)
                    {
                        gt = "Nu";
                    }
                    kn.add_KhachHang(ma, ten, gioitinh, ngaysinh, sdt, diachi);
                    dgvDanhsachKH.DataSource = kn.select_pro("select_KhachHang");
                    xoachu();
                }
            }
        }

        private void dgvDanhsachKH_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int r = dgvDanhsachKH.CurrentCell.RowIndex;
            btnThem.Enabled = false;
            txtMaKH.Enabled = false;
            txtMaKH.Text = dgvDanhsachKH.Rows[r].Cells["MaKH"].Value.ToString();
            txtTenKH.Text = dgvDanhsachKH.Rows[r].Cells["TenKH"].Value.ToString();
            if (dgvDanhsachKH.Rows[r].Cells["GioiTinh"].Value.ToString() == "Nam")
            {
                rdNam.Checked = true;
            }
            else
            {
                rdNu.Checked = true;
            }
            dtNgaysinh.Text = dgvDanhsachKH.Rows[r].Cells["NgaySinh"].Value.ToString();

            txtSDT.Text = dgvDanhsachKH.Rows[r].Cells["DienThoai"].Value.ToString();
            txtDiachi.Text = dgvDanhsachKH.Rows[r].Cells["DiaChi"].Value.ToString();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            string ma = txtMaKH.Text;
            string ten = txtTenKH.Text;
            string sdt = txtSDT.Text;
            string diachi = txtDiachi.Text;
            string ngaysinh = dtNgaysinh.Value.ToString("dd/MM/yyyy");
            string gioitinh = rdNam.Checked ? "Nam" : "Nữ";
            if (txtMaKH.Text == "" || txtTenKH.Text == " ")
            {
                MessageBox.Show("Chưa Nhập Mã Khách Hàng với Tên Khách Hàng");
                txtMaKH.Focus();
            }
            else
            {
                DataTable dt = new DataTable();
                dt = kn.Truyvan("select * from tbl_NhanVien where MaNV = '" + txtMaKH.Text + "'");
                if (dt.Rows.Count > 0)
                {
                    MessageBox.Show("Đã tồn tại mã khách hàng");
                }
                else
                {
                    string gt = "Nam";
                    if (rdNam.Checked == true)
                    {
                        gt = "Nu";
                    }
                    kn.update_KhachHang(ma, ten, gioitinh, ngaysinh, sdt, diachi);
                    dgvDanhsachKH.DataSource = kn.select_pro("select_KhachHang");
                    xoachu();
                }
            }
        }
        public void xoachu()
        {
            btnThem.Enabled = true;
            txtMaKH.Enabled = true;
            txtTenKH.ResetText();
            txtSDT.ResetText();
            txtDiachi.ResetText();
            rdNam.Checked = true;
        }

        private void btnxoa_Click(object sender, EventArgs e)
        {
            if (txtMaKH.Text == "")
            {
                MessageBox.Show("Mời Bạn Nhập Mã Cần Xóa ");
            }
            else
            {
                kn.delete_KhachHang(txtMaKH.Text);
                dgvDanhsachKH.DataSource = kn.select_pro("select_KhachHang");
                xoachu();
            }
        }

        private void btnlammoi_Click(object sender, EventArgs e)
        {
            dgvDanhsachKH.DataSource = kn.select_pro("select_KhachHang");
            xoachu();
        }

        private void btntimkiem_Click(object sender, EventArgs e)
        {
            dgvDanhsachKH.DataSource = kn.timkiem_KhachHang(txttimkiem.Text);
        }

        private void txttimkiem_KeyUp(object sender, KeyEventArgs e)
        {
            btntimkiem_Click(sender, e);
        }
    }
}
