﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLCuaHang
{
    public partial class HangHoa : Form
    {
        public HangHoa()
        {
            InitializeComponent();
        }
        KetnoiHanghoa kn = new KetnoiHanghoa();
        private void HangHoa_Load(object sender, EventArgs e)
        {
            dgvDanhsachMH.DataSource = kn.select_pro("select_HangHoa");
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            //txtmaHang.Enabled = false;
            string ma = txtmaHang.Text;
            string ten = txttenHang.Text;
            string soluong = txtSoluonghang.Text;
            string donvitinh = txtDVT.Text;
            string gia = txtGia.Text;

            kn.add_HangHoa(ma,ten, soluong, donvitinh, gia);
            dgvDanhsachMH.DataSource = kn.select_pro("select_HangHoa");
            resetform();
        }

        private void dgvDanhsachMH_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int r = dgvDanhsachMH.CurrentCell.RowIndex;
            //txtmaHang.Enabled = false;
            txtmaHang.Text = dgvDanhsachMH.Rows[r].Cells["MaHang"].Value.ToString();
            txttenHang.Text = dgvDanhsachMH.Rows[r].Cells["TenHang"].Value.ToString();
            txtSoluonghang.Text = dgvDanhsachMH.Rows[r].Cells["SoLuongHang"].Value.ToString();
            txtDVT.Text = dgvDanhsachMH.Rows[r].Cells["DonViTinh"].Value.ToString();
            txtGia.Text = dgvDanhsachMH.Rows[r].Cells["Gia"].Value.ToString();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            string ma = txtmaHang.Text;
            string ten = txttenHang.Text;
            string soluong = txtSoluonghang.Text;
            string donvitinh = txtDVT.Text;
            string gia = txtGia.Text;

            kn.update_HangHoa(ma, ten, soluong, donvitinh, gia);
            dgvDanhsachMH.DataSource = kn.select_pro("select_HangHoa");
            resetform();
        }
        private void resetform()
        {
            txtmaHang.ResetText();
            txttenHang.ResetText();
            txtSoluonghang.ResetText();
            txtDVT.ResetText();
            txtGia.ResetText();
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            txtmaHang.Enabled = false;
            txttenHang.Enabled = false;
            txtSoluonghang.Enabled = false;
            txtDVT.Enabled = false;
            txtGia.Enabled = false;

            btnSua.Enabled = false;
            btnThem.Enabled = true;
        }

        private void btnlammoi_Click(object sender, EventArgs e)
        {
            resetform();
            dgvDanhsachMH.DataSource = kn.select_pro("select_HangHoa");
        }

        /*public string temp;

        private void btninds_Click(object sender, EventArgs e)
        {
            form f = new form();
           
         
           f.temp = txtGia.Text;

            f.Show();

        }*/
        private void btninds_Click(object sender, EventArgs e)
        {
            InDSHH f = new InDSHH();
            f.Show();
        }

    }
}
