﻿namespace QLCuaHang
{
    partial class InDSHD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rpHoadon = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // rpHoadon
            // 
            this.rpHoadon.ActiveViewIndex = -1;
            this.rpHoadon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rpHoadon.Cursor = System.Windows.Forms.Cursors.Default;
            this.rpHoadon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rpHoadon.Location = new System.Drawing.Point(0, 0);
            this.rpHoadon.Name = "rpHoadon";
            this.rpHoadon.Size = new System.Drawing.Size(884, 461);
            this.rpHoadon.TabIndex = 0;
            // 
            // InDSHD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 461);
            this.Controls.Add(this.rpHoadon);
            this.Name = "InDSHD";
            this.Text = "InDSHD";
            this.Load += new System.EventHandler(this.InDSHD_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer rpHoadon;
    }
}