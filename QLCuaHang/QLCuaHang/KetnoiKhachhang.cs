﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace QLCuaHang
{
    class KetnoiKhachhang
    {
        // public static string Ketnoi = @"Data Source=DESKTOP-171AVQP\SQLEXPRESS;Initial Catalog=QLCuaHang1;Integrated Security=True";
        public static string Ketnoi = @"Data Source=DESKTOP-FSTUGSU\SQLEXPRESS;Initial Catalog=QLCuaHang1;Integrated Security=True";

        SqlConnection connection;
        DataTable dt;
        SqlDataAdapter da;
        SqlCommand command;
        public SqlConnection open()
        {
            connection = new SqlConnection(Ketnoi);
            if (connection.State == ConnectionState.Closed)
                connection.Open();
            return connection;
        }
        public SqlConnection closed()
        {
            connection = new SqlConnection(Ketnoi);
            if (connection.State == ConnectionState.Open)
                connection.Close();
            return connection;
        }
        public DataTable Truyvan(string sql)
        {
            dt = new DataTable();
            command = new SqlCommand(sql, connection);
            da = new SqlDataAdapter(command);
            da.Fill(dt);
            return dt;
        }
        public DataTable select_pro(string tenproc)
        {
            dt = new DataTable();
            open();
            command = new SqlCommand(tenproc, connection);
            command.CommandType = CommandType.StoredProcedure;
            da = new SqlDataAdapter(command);
            da.Fill(dt);
            closed();
            return dt;
        }
        public SqlCommand add_KhachHang(string manv, string tennv, string gioitinh, string ngaysinh, string sdt, string diachi)
        {
            open();
            command = new SqlCommand("add_KhachHang", connection);
            command.Parameters.Add(new SqlParameter("@makh", manv));
            command.Parameters.Add(new SqlParameter("@tenkh", tennv));
            command.Parameters.Add(new SqlParameter("@gioitinh", gioitinh));
            command.Parameters.Add(new SqlParameter("@ngaysinh", ngaysinh));
            command.Parameters.Add(new SqlParameter("@sdt", sdt));
            command.Parameters.Add(new SqlParameter("@diachi", diachi));
            command.CommandType = CommandType.StoredProcedure;
            command.ExecuteNonQuery();
            closed();
            return command;
        }
        public SqlCommand update_KhachHang(string makh, string tenkh, string gioitinh, string ngaysinh, string sdt, string diachi)
        {
            open();
            command = new SqlCommand("update_KhachHang", connection);
            command.Parameters.Add(new SqlParameter("@makh", makh));
            command.Parameters.Add(new SqlParameter("@tenkh", tenkh));
            command.Parameters.Add(new SqlParameter("@gioitinh", gioitinh));
            command.Parameters.Add(new SqlParameter("@ngaysinh", ngaysinh));
            command.Parameters.Add(new SqlParameter("@sdt", sdt));
            command.Parameters.Add(new SqlParameter("@diachi", diachi));
            command.CommandType = CommandType.StoredProcedure;
            command.ExecuteNonQuery();
            closed();
            return command;
        }
        public SqlCommand delete_KhachHang(string manv)
        {
            open();
            SqlCommand command = new SqlCommand("delete_KhachHang", connection);
            command.Parameters.Add(new SqlParameter("@makh", manv));
            command.CommandType = CommandType.StoredProcedure;
            command.ExecuteNonQuery();
            closed();
            return command;
        }
        public DataTable timkiem_KhachHang(string noidung)
        {
            dt = new DataTable();
            open();
            SqlCommand command = new SqlCommand("timkiemKH", connection);
            command.Parameters.Add(new SqlParameter("@noidung", noidung));
            command.CommandType = CommandType.StoredProcedure;
            da = new SqlDataAdapter(command);
            da.Fill(dt);
            closed();
            return dt;
        }
    }
}
